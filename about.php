<div class="tab-pane" id="tab3">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Manage About</span>
        </div>
        <div class="actions" style="margin-top:0px;">
            <a type="button" class="btn green" data-toggle="modal" href="#add-about">
                <img src="assets/img/add_selection.png"> Add About
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Search :</label>
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-12">
            <div class="m-grid m-grid-demo">
                <div class="m-grid-row">
                    <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-1 col-right-border ">
                        <img src="assets/img/schedule.png" alt="">
                    </div>
                    <div class="m-grid-col m-grid-col-middle m-grid-col-md-9 col-padding">
                        <h4 style="margin-top: 0px;margin-bottom: 0px;" class="color">About Eventify</h4>
                        <p style="margin-top: 0px;margin-bottom: 0px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                    </div>
                    <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-2 col-left-border">
                        <div>
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="add-about" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form action="#" class="form-horizontal form-bordered">
                    <div class="form-body">
                        <div class="form-group last">
                            <div class="col-md-12">
                                <textarea class="ckeditor form-control" name="editorabout" rows="10" style="box-shadow:none !important;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-10">
                                <a href="" class="btn green">
                                    <i class="fa fa-check"></i> Submit
                                </a>
                                <a href="" class="btn btn-outline grey-salsa">
                                                                Cancel
                                                            </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
                                
<script>

     CKEDITOR.replace( 'editorabout', {
        language: 'en',
        uiColor : '#303061',
        colorButton_enableMore : 'true',
        resize_enabled: false,
        removePlugins : 'format, iframe, pagebreak, div, specialchar',
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
                                { name: 'basicstyles', groups: [ 'basicstyles'] },
                                { name: 'paragraph',   groups: [ 'list',  'blocks', 'align' ] },
                                { name: 'links' },
                                { name: 'insert' },
                                { name: 'styles', groups: [ 'Font',  'Size'] },
                                { name: 'colors' }
                       ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'Smiley,iFrame,Flash,Strike,Subscript,Superscript,Anchor,Styles,specialchar,HorizontalRule,Table'
        } );
     
     CKEDITOR.on('instanceReady', function () {
                                $('.cke_top').css('background','transparent');
                                $('.cke_bottom').css('background','transparent');
                                $('.cke_button_on').css('background','#5a5aad');
                                $('.cke_bottom').css('border-top-color','transparent');
                                $('.cke_combo_button').css('border-color','transparent');
                                $('.cke_combo_button').css('background','transparent');
                                $('.cke_toolgroup').css('border-color','transparent');
                                $('.cke_toolgroup').css('background','transparent');
                                $('.cke_top').css('border-color','#e3e3e3');
                                $('.cke_chrome').css('border-color','#e3e3e3');
                                $('.cke_chrome').css('filter','none');
                                $('.cke_toolgroup').css('filter','none');
     });
     
</script>