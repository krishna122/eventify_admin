<div class="row">
    <div class="col-md-12">
        <div class="col-md-8">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form action="#" id="" novalidate="novalidate">
                        <div class="form-body">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <span class="caption-subject bold uppercase">App Store Details</span>
                                    <span style="color: #FF885D;font-size: 10px;">
                                        &nbsp;<i class="icon-question"></i>
                                        Heighlight all the tabs/content you'd like to have it in the app
                                    </span>
                                    <p class="theme-text-color">Attention: Settings here cannot be updated instantly once your app is in the appstore.</p>
                                </div>
                                <div class="actions" style="margin-top: -62px;">
                                    <a class="btn btn-sm green"> Submit</a>
                                </div>
                            </div>
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase theme-text-color">About the app</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >App Name :</label>
                                        <div class="input-icon input-icon-sm right">
                                            <i>30</i>
                                            <input class="form-control input-sm" type="text" class="form-control" name="appname">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Icon Label :</label>
                                        <div class="input-icon input-icon-sm right">
                                            <i>12</i>
                                            <input class="form-control input-sm" type="text" class="form-control" name="appname">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase theme-text-color">Description</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" col="3" placeholder="Describe your event, organization, or purpose for creating this app. Remember your description will be included in the App Store and Google Play"
                                                  style=" border-left: 1px solid #E3E3E3 !important; border-right: 1px solid #E3E3E3 !important; border-top: 1px solid #E3E3E3 !important; height: auto !important;" ></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="portlet-title row-margin">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase theme-text-color">Event Lock</span>
                                    <p class="theme-text-color">Lock your app with an event code (password) to restrict access. The event code can be updated anytime.</p>
                                </div>
                                <div class="actions" style="margin-top: -62px;">
                                    <!-- Rounded switch -->
                                    <label class="switch">
                                        <input type="checkbox">
                                        <div class="slider round" style="margin: 0px 0px !important;"></div>
                                    </label> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label >Enter event code :</label>
                                        <div class="input-icon input-icon-sm right">
                                            <input class="form-control input-sm" type="text" class="form-control" name="appname">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Enter code :</label>
                                        <div class="input-icon input-icon-sm right">
                                            <input class="form-control input-sm" type="text" class="form-control" name="appname">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row row-margin margin-bottom">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase theme-text-color">App Icon</span>
                                    <small>&nbsp; 1024x1024</small>
                                </div>
                            </div>
                            <form class="dropzone dropzone-file-area" id="appic" style="width: 200px; height: 200px;">
                            </form>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase theme-text-color">iPhone 6 - 7</span>
                                    <small>&nbsp; 750x1334</small>
                                </div>
                            </div>
                            <form class="dropzone dropzone-file-area" id="ip67" style="width: 200px; height: 300px;">
                            </form>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject bold uppercase theme-text-color">iPhone 4s</span>
                                    <small>&nbsp; 640x960</small>
                                </div>
                            </div>
                            <form action="/upload" class="dropzone dropzone-file-area" drop-zone="" id="ip4" style="width: 200px; height: 250px;"></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="portlet light bordered" style="height: 730px; width: 100%;">
            </div>
        </div>
    </div>
</div>
  

<script>
//DropzoneJS snippet - js
$.getScript('assets/layouts/layout2/scripts/dropzone.js',function(){
  // instantiate the uploader
  $('#appic').dropzone({ 
    url: "upload",
    maxFilesize: 100,
    paramName: "uploadfile",
    maxThumbnailFilesize: 5,
    init: function() {
      
      //this.on('success', function(file, json) {
      //});
      //
      //this.on('addedfile', function(file) {
      //  
      //});
      //
      //this.on('drop', function(file) {
      //  
      //}); 
    }
  });
});

//DropzoneJS snippet - js
$.getScript('assets/layouts/layout2/scripts/dropzone.js',function(){
  // instantiate the uploader
  $('#ip67').dropzone({ 
    url: "upload",
    maxFilesize: 100,
    paramName: "uploadfile",
    maxThumbnailFilesize: 5,
    init: function() {
      
      //this.on('success', function(file, json) {
      //});
      //
      //this.on('addedfile', function(file) {
      //  
      //});
      //
      //this.on('drop', function(file) {
      //  alert('file');
      //}); 
    }
  });
});

//DropzoneJS snippet - js
$.getScript('assets/layouts/layout2/scripts/dropzone.js',function(){
  // instantiate the uploader
  $('#ip4').dropzone({ 
    url: "upload",
    maxFilesize: 100,
    paramName: "uploadfile",
    maxThumbnailFilesize: 5,
    init: function() {
      
      //this.on('success', function(file, json) {
      //});
      //
      //this.on('addedfile', function(file) {
      //  
      //});
      //
      //this.on('drop', function(file) {
      //  alert('file');
      //}); 
    }
  });
});

$(document).ready(function() {});
</script>
