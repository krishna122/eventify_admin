<div class="tab-pane" id="tab8">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Web View</span>
            <p class="color">You can link your website or use the HTML editor to create one</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                <label >Website Link :</label>
                <div class="input-icon input-icon-sm right">
                    <input class="form-control input-sm" type="url" class="form-control" name="weblink">
                </div>
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-12">
			<form action="#" class="form-horizontal form-bordered">
				<div class="form-body">
                    <div class="form-group last">
                        <div class="col-md-12">
                            <textarea class="ckeditor form-control" name="editor1" rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-10">
                            <a href="" class="btn green">
                                <i class="fa fa-check"></i> Submit
                            </a>
                            <a href="" class="btn btn-outline grey-salsa">
                                Cancel
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

     CKEDITOR.replace( 'editor1', {
        language: 'en',
		uiColor : '#303061',
		colorButton_enableMore : 'true',
		resize_enabled: false,
		removePlugins : 'format, iframe, pagebreak, div, specialchar',
			// Define the toolbar groups as it is a more accessible solution.
			toolbarGroups: [
				{ name: 'basicstyles', groups: [ 'basicstyles'] },
				{ name: 'paragraph',   groups: [ 'list',  'blocks', 'align' ] },
				{ name: 'links' },
				{ name: 'insert' },
				{ name: 'styles', groups: [ 'Font',  'Size'] },
				{ name: 'colors' }
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Smiley,iFrame,Flash,Strike,Subscript,Superscript,Anchor,Styles,specialchar,HorizontalRule,Table'
		} );
	 
	 CKEDITOR.on('instanceReady', function () {
		$('.cke_top').css('background','transparent');
		$('.cke_bottom').css('background','transparent');
		$('.cke_button_on').css('background','#5a5aad');
		$('.cke_bottom').css('border-top-color','transparent');
		$('.cke_combo_button').css('border-color','transparent');
		$('.cke_combo_button').css('background','transparent');
		$('.cke_toolgroup').css('border-color','transparent');
		$('.cke_toolgroup').css('background','transparent');
		$('.cke_top').css('border-color','#e3e3e3');
		$('.cke_chrome').css('border-color','#e3e3e3');
		
	  });
     
</script>