<div class="row">
    <div class="col-lg-1 col-md-1"></div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Posts</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Photos</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Likes</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Comments</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>TOTAL Outreach</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-1 col-md-1"></div>
</div>



<div class="portlet light"  style="margin-top: 15px;">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Activity feed preview</span>
        </div>
    </div>

  <div class="portlet-body">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr style="border:none;">
            <th width="30%"> Latest Post 
            </th>
            <th> Outreach 
            </th>
            <th class="numeric"> Likes 
            </th>
            <th class="numeric"> Comments 
            </th>
            <th class="numeric"> Reported 
            </th>
            <th class="numeric"> 
            </th>
            <th class="numeric"> 
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> 
              <div class="item-details">
                <img class="item-pic rounded" src="assets/pages/media/users/avatar4.jpg">
                <a href="" class="item-name primary-link">Nick Larson
                </a>
                <p style="margin-left: 44px; margin-top: -14px;" class="font-red-sunglo">The last speaker was out of this world
                </p>
              </div>
            </td>
            <td>100 
            </td>
            <td class="numeric"> 50 
            </td>
            <td class="numeric"> 300  
            </td>
            <td class="numeric"> 10000  
            </td>
            <td class="numeric">
              <center>
                <a class="btn-group" style="font-size: 25px;">
                  <img src="assets/img/view.png">
                  </i>
                </a>
              </center>
            </td>
            <td class="numeric">
              <center>
                <a class="btn-group" style="font-size: 22px;">
                  <img src="assets/img/cross.png">
                  </i>
                </a>
              </center>
            </td>
          </tr>
          <tr>
            <td> 
              <div class="item-details">
                <img class="item-pic rounded" src="assets/pages/media/users/avatar4.jpg">
                <a href="" class="item-name primary-link">Nick Larson
                </a>
                <p style="margin-left: 44px; margin-top: -14px;" class="font-red-sunglo">The last speaker was out of this world
                </p>
              </div>
            </td>
            <td>100 
            </td>
            <td class="numeric"> 50 
            </td>
            <td class="numeric"> 300  
            </td>
            <td class="numeric"> 10000  
            </td>
            <td class="numeric">
              <center>
                <a class="btn-group" style="font-size: 25px;">
                  <img src="assets/img/view.png">
                  </i>
                </a>
              </center>
            </td>
            <td class="numeric">
              <center>
                <a class="btn-group" style="font-size: 22px;">
                  <img src="assets/img/cross.png">
                  </i>
                </a>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
