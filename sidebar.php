<!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item active">
                            <a href="features.php" class="ajaxify nav-link">
                                <img src="assets/img/feature.png" alt="">
                                <span class="title">Feautres</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="content.php" class="ajaxify nav-link">
                                <img src="assets/img/content.png" alt="">
                                <span class="title">Content</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="settings.php" class="ajaxify nav-link">
                                <img src="assets/img/setting.png" alt="">
                                <span class="title">Settings</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="dashboard.php" class="ajaxify nav-link">
                                <img src="assets/img/dashboard.png" alt="">
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="activityfeed.php" class="ajaxify nav-link">
                                <img src="assets/img/activity_feed.png" alt="">
                                <span class="title">Activity Feed</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="notification.php" class="ajaxify nav-link">
                                <img src="assets/img/notification.png" alt="">
                                <span class="title">Notifications</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="analytics.php" class="ajaxify nav-link">
                                <img src="assets/img/analytics.png" alt="">
                                <span class="title">Analytics</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="audience.php" class="ajaxify nav-link">
                                <img src="assets/img/audience.png" alt="">
                                <span class="title">Audience</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->