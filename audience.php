<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Sessions</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Registered Users</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <form role="form">
                <div class="form-body">
                    <div class="form-group form-md-line-input has-error">
                        <div class="input-group">
                            <div class="input-group-control">
                                <input type="text" class="form-control" placeholder="Seach for user..">
                            </div>
                            <span class="input-group-btn btn-right">
                                <a type="button" class="btn btn-primary green"> FIND</a>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="portlet light row-margin">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">User Management</span>
        </div>
        <div class="actions" style="margin-top: 8px;">
            <form id="fileupload" action="" method="POST" enctype="multipart/form-data" class="">
                <div class="fileupload-buttonbar">
                    <span class="btn green fileinput-button">
                      <i class="icon-cloud-upload">
                      </i>
                      <span> Import users 
                      </span>
                      <input name="files[]" multiple="" type="file" style="display:none;"> 
                    </span>
                </div>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped clearfix">
                  <tbody class="files"> 
                  </tbody>
                </table>
            </form>

        </div>
    </div>

  <div class="portlet-body">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr style="border:none;">
            <th width="30%"> User in order of registration
            </th>
            <th> Outreach 
            </th>
            <th class="numeric"> Email  
            </th>
            <th class="numeric"> Phone 
            </th>
            <th class="numeric"> Interaction 
            </th>
            <th class="numeric"> 
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> 
              <div class="item-details">
                <img class="item-pic rounded" src="assets/pages/media/users/avatar4.jpg">
                <a href="" class="item-name primary-link">Annie Scultz
                </a>
                <p style="margin-left: 44px; margin-top: -14px;" class="font-red-sunglo">10/10/2016
                </p>
              </div>
            </td>
            <td>100 
            </td>
            <td class="numeric"> Anns@gmail.com 
            </td>
            <td class="numeric"> 785-564-2244  
            </td>
            <td class="numeric"> 24  
            </td>
            <td class="numeric">
              <center>
                <a class="btn-group" style="font-size: 25px;">
                  <img src="assets/img/cross.png">
                  </i>
                </a>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
