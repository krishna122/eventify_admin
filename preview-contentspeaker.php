
<div class="col-sm-12 thumbnailP" id="ptab1">
    <div class="navbar-preview">
        <a style="color:#818592; font-size:20px; float:left;">
            <i class="fa fa-bars"></i>
        </a> 
       <center> <a style="color:#818592; font-size:20px;">
           About
        </a> 
           </center>
        <a style="color:#818592; font-size:20px; float:right;margin-top: -26px;">
           <i class="fa fa-search" style="margin-right:15px;"></i>
           <i class="fa fa-bookmark"></i>    
        </a>
    </div>
    <img alt="" class="img-responsive" src="assets/img/content-speaker-background.jpg">
    <div class="caption-preview">
        <div class="captionD">
            <h4>Jonathan Miller</h4>
            <p><span style="border:1px solid #949494;border-radius:5px !important;padding:5px;">Senior Speaker</span></p>
            <p><strong style="color:#fff; font-size:20px; ">Microsoft</strong></p>
            <center>
                <a>
                    <img src="assets/img/arrow_ani.png" class="image-responsive">
                </a>
            </center>    
        </div>
       
        <div class="captionP text-center">
            <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-facebook tooltips" data-original-title="Facebook" style="color:#4d74c1; border:1px solid #4d74c1;"></a>
            <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-linkedin tooltips" data-original-title="Linkedin" style="color:#a0a0a0;"></a>
            <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-twitter tooltips" data-original-title="Twitter" style="color:#a0a0a0;"></a>
            <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-google tooltips" data-original-title="Google" style="color:#a0a0a0;"></a>
        </div>

    </div>
<!--
    <div id="speaker-details">
        <img alt="" class="img-responsive" src="assets/img/content-speaker-background.jpg" style="-webkit-filter: blur(10px);-moz-filter: blur(10px);-o-filter: blur(10px);-ms-filter: blur(10px);filter: blur(10px);">
        <div class="caption-preview">
            <div class="captionD" style="float:left;width:430px;overflow-x:auto;padding-left:0px;">
                <ul>
                    <li style="display:inline;color:#f44f4d;">
                        <img src="assets/img/pdf_icon@3x.png"><br>
                        <a>Some dummy text</a><br>
                        <a>Some dummy text</a>
                    </li>
                    <li style="display:inline;color:#2591e5;">
                        <img src="assets/img/slid_show_icon@3x.png"><br>
                        <a>Some dummy text</a><br>
                        <a>Some dummy text</a>
                    </li>
                    <li style="display:inline;color:#f87626;">
                        <img src="assets/img/word_icon@3x.png">
                    </li>
                    <li style="display:inline;color:#f87626;">
                        <img src="assets/img/zip_icon_01@3x.png">
                    </li>
                </ul>
            </div>
            <div class="captionP text-center">
                <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-facebook tooltips" data-original-title="Facebook" style="color:#4d74c1; border:1px solid #4d74c1;"></a>
                <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-linkedin tooltips" data-original-title="Linkedin" style="color:#a0a0a0;"></a>
                <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-twitter tooltips" data-original-title="Twitter" style="color:#a0a0a0;"></a>
                <a href="#" class="socicon-btn socicon-btn-circle socicon-lg socicon-google tooltips" data-original-title="Google" style="color:#a0a0a0;"></a>
            </div>
        </div>
    </div>
-->
    
</div>