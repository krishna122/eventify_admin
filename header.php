<div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="./">
                        <img src="assets/layouts/layout2/img/logo-default.png" alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                        
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username"> Krishna Kumar Dipak </span>
                                    <img alt="" class="img-circle" src="assets/layouts/layout2/img/avatar3_small.jpg" />
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <i class="icon-info"></i> FAQs
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <i class="icon-question"></i> Help
                                        </a>
                                    </li>
                                    <li class="divider"> </li>          
                                    <li>
                                        <a href="">
                                            <i class="icon-power"></i> Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            
                            <!-- BEGIN LANGUAGE BAR -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-language">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" src="assets/global/img/flags/us.png">
                                    <span class="langname"> US </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="javascript:;">
                                            <img alt="" src="assets/global/img/flags/es.png"> Spanish </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <img alt="" src="assets/global/img/flags/de.png"> German </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <img alt="" src="assets/global/img/flags/ru.png"> Russian </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <img alt="" src="assets/global/img/flags/fr.png"> French </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END LANGUAGE BAR -->
                            
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->