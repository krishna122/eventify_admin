<div class="tab-pane" id="tab5">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <span class="caption-subject bold uppercase">Manage Map</span>
                                    </div>
                                    <div class="actions" style="margin-top:0px;">
                                        <a class="btn green" data-toggle="modal" data-target="#Map"><i class="icon-map"></i> Add Map</a>
                                        <a class="btn green" data-toggle="modal" data-target="#Intactivemap"><i class="icon-pointer"></i> Add Interactive Map</a>
                                    </div>
                                    <p>You can create a simple map or upload your own interactive map</p>
                                </div>
                                <div class="row row-margin">
                                    <div class="col-md-12">
                                        <div class="m-grid m-grid-demo">
                                            <div class="m-grid-row">
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-1 col-right-border ">
                                                    <img src="assets/img/schedule.png" alt="">
                                                </div>
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-md-9 col-padding">
                                                    <h4 style="margin-top: 0px;margin-bottom: 0px;" class="color">Facebook Inc.</h4>
                                                    <p style="margin-top: 0px;margin-bottom: 0px;">
                                                        <span>Booth: 034</span> &nbsp;&nbsp; <span>Phone: 448 556 6633</span> &nbsp;&nbsp; <span>Email: email@facebook.com</span>
                                                    </p>
                                                </div>
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-2 col-left-border">
                                                   <div>
                                                        <a><img src="assets/img/edit.png"></a>
                                                        <a><img src="assets/img/close.png"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-margin">
                                    <div class="col-md-12">
                                        <div class="m-grid m-grid-demo">
                                            <div class="m-grid-row">
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-1 col-right-border ">
                                                    <img src="assets/img/schedule.png" alt="">
                                                </div>
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-md-9 col-padding">
                                                    <h4 style="margin-top: 0px;margin-bottom: 0px;" class="color">Facebook Inc.</h4>
                                                    <p style="margin-top: 0px;margin-bottom: 0px;">
                                                        <span>Booth: 034</span> &nbsp;&nbsp; <span>Phone: 448 556 6633</span> &nbsp;&nbsp; <span>Email: email@facebook.com</span>
                                                    </p>
                                                </div>
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-2 col-left-border">
                                                   <div>
                                                        <a><img src="assets/img/edit.png"></a>
                                                        <a><img src="assets/img/close.png"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


<!-- Map Modal -->
<div id="Map" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Map Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title color">Add Map</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
               <div class="col-md-8">
                  <div class="form-group">
                     <label>Name: </label>
                     <input type="text" class="form-control">           
                  </div>
                  <div class="form-group">
                     <label>Address: </label>
                     <input type="text" class="form-control">           
                  </div>
                  <div class="form-group">
                  <div class="row">
                     <div class="col-md-4">
                       <label>City :</label>
                       <input class="form-control" type="text" name="">
                    </div>
                   <div class="col-md-4">
                      <label>State :</label>
                      <input class="form-control" type="text" name="">
                   </div>       
                   <div class="col-md-4">
                      <div class="form-group">
                         <label>Zip :</label>
                         <input class="form-control" type="text" name="">
                      </div>          
                   </div>
                </div>
              </div>
              <div class="form-group">
                 <label>Map :</label><br><br>
                 <div class="portlet box">
                    <!--<div class="portlet-title" style="background: #282E53;">
                        <div class="caption" style="font-size: 14px;">
                           <i class="icon-cursor-move" style="color: #9C9DC4;"></i>
                           Drag & drop Pin to Adjust Location
                        </div>
                    </div> -->           
                    <div class="portlet-body" style="padding: 0px;">
                       <iframe
                                width="100%"
                                height="450"
                                frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDrK9Dvem1t4oMh0xpUCsx3MnKoi0A36Pc&q=Space+Needle,Seattle+WA" allowfullscreen>
                       </iframe>
                    </div>
                 </div>              
              </div>    
            </div>
           <div class="col-md-4">
      
           </div>
        </div>
      </div>
   </div>
</div>
</div>
</div>




<!--Interactive Map Modal -->
<div id="Intactivemap" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Interactive Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title color">Add Interactive Map</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
               <div class="col-md-8">
                  <div class="form-group">
                     <label>Name: </label>
                     <input type="text" class="form-control">           
                  </div>
                  <div class="form-group">
                       <label>Map :</label><br><br>
                      <div class="portlet box">
                         <div class="portlet-title" style="background: #282E53;">
                            <div class="caption">
                               <a class="btn btn-default" data-toggle="modal" data-target="#location" style="line-height: 0;">
                                <i class="icon-plus"></i> Location
                               </a>
                            </div>
                            <div class="actions" style="margin-top:3px; ">
                                <input id="upload" type="file">
                                <a class="btn btn-default" id="upload_link">Choose Files </a>
                            </div>
                         </div>       
                        <div class="portlet-body dragscroll" style="padding: 0px;width:100%;height:400px;overflow: auto;">
                             <img src="#" id="interactivemap">
                       </div>
                        <div class="portlet-footer" style="background: #282E53;padding: 10px;">
                             <div class="caption">
                               <button onclick="zoomin();" type="button" class="btn btn-default" style="line-height: 0 !important;"><i class="icon-magnifier-add"></i> zoom-in</button>
                               <button onclick="zoomout();" type="button" class="btn btn-default" style="line-height: 0 !important;"><i class="icon-magnifier-remove"></i> zoom-out</button>
                            </div>   
                        </div>        
                     </div>              
                 </div>
                  <div class="form-group">
                       <a class="btn green pull-right">Add Map</a>            
                  </div>
              </div>
             <div class="col-md-4">
                                
             </div>
            </div>
        </div>
      </div>
    </div>
    
    
    <!--Location Form-->
    
    <div class="modal fade" id="location" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <div class="form-group">
                <label>Location Name: </label>
                <input type="text" class="form-control">           
          </div>
          <div class="form-group">
                <label>Color: </label>
                <input type="text" class="form-control colorpicker">           
           </div>
           <div class="form-group">
                <label>Booth Number: </label>
                <input type="text" class="form-control">           
           </div>  
          <div class="form-group">
                <label>Link to: </label>
                <input type="text" class="form-control">           
            </div>
        </div>
        <div class="modal-footer">
          <a class="btn green">Save</a>
        </div>
      </div>
      
    </div>
  </div>

  </div>
</div>


<script>
//color picker
    $('.colorpicker').colorpicker();
</script>

<script>
$(function(){
   $("#upload_link").on('click', function(e){
   e.preventDefault();
   $("#upload:hidden").trigger('click'); 
  });
});

function readURL(input) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
      $('#interactivemap').attr('src', e.target.result);
      };
    reader.readAsDataURL(input.files[0]);
   }
 }
$("#upload").change(function(){
   readURL(this);
});

</script>

<script type="text/javascript">
    function zoomin(){
        var myImg = document.getElementById("interactivemap");
        var currWidth = myImg.clientWidth;
        if(currWidth == 1024){
            alert("Maximum zoom-in level reached.");
        } else{
            myImg.style.width = (currWidth + 50) + "px";
        } 
    }
    function zoomout(){
        var myImg = document.getElementById("interactivemap");
        var currWidth = myImg.clientWidth;
        if(currWidth == 1000){
            alert("Maximum zoom-out level reached.");
        } else{
            myImg.style.width = (currWidth - 50) + "px";
        }
    }
    
</script>
