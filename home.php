<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    
    <!-- BEGIN HEAD -->
    <head>
        <?php include "head.php"; ?>
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        
        <!-- BEGIN HEADER -->
        <?php include "header.php" ?>
        <!-- END HEADER -->
        
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            
            <?php include "sidebar.php" ?>
            
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    
                    <?php //include "themecolorchanger.php" ?>
                    
                    <div class="page-content-body" id="contentBody">
                        
                        <!-- Page Will load through Ajax -->
                        
                    </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> &copy; <?php echo date("Y"); ?> Eventify.io All right reserved.
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>
        <!-- END FOOTER -->
        
        <?php include "script.php"; ?>
        
        <?php include "customscript.php"; ?>
        
    </body>
</html>