<div class="tab-pane active" id="tab1">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Manage Speakers</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-5">
            <label>Search :</label>
            <input type="text" class="form-control">
        </div>
        <div class="col-md-6 col-sm-7">
            <div class="pull-right">
                <a type="button" class="btn green">
                    <img src="assets/img/export_cv.png"> Export to CSV
                </a>
                <a type="button" class="btn green" data-toggle="modal" href="#add-speaker   ">
                    <img src="assets/img/add_selection.png"> Add Speaker
                </a>
            </div>
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top" style="margin-top: -31px;">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--MODAL OF ADD SPEAKER-->

   <div id="add-speaker" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                                <!-- BEGIN FORM-->
                                <form action="#" id="form_sample_3" novalidate="novalidate">
                                    <div class="form-body">
                                        <div class="portlet-title">
                                            <div class="caption font-red-sunglo">
                                                <span class="caption-subject bold uppercase">New Speaker</span>
                                            </div>
                                            <div class="actions">
                                                <a class="btn btn-sm green"> Cancel</a>
                                                <a class="btn btn-sm green"> Create</a>
                                            </div>
                                        </div>
                                        <div class="portlet-title">
                                            <div class="caption color">
                                                <span class="caption-subject bold uppercase">Persional details</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="form_control_1">First Name : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="form_control_1">Last Name : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="row row-margin">
                                            <div class="col-md-6">
                                                <label for="form_control_1">Position : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="form_control_1">Company : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="row row-margin">
                                            <div class="col-md-6">
                                                <label for="form_control_1">Email : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="form_control_1">Phone : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="row row-margin">
                                            <div class="col-md-6">
                                                <label for="form_control_1">Facebook : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="form_control_1">LinkedIn : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="row row-margin">
                                            <div class="col-md-6">
                                                <label for="form_control_1">Google+ : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="form_control_1">Twitter : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="portlet-title row-margin">
                                            <div class="caption">
                                                <span class="caption-subject bold uppercase theme-text-color">Description</span>
                                            </div>
                                        </div>
                                        <div class="row row-margin">
                                            <div class="col-md-12">
                                                <textarea class="form-control" rows="3" col="3"
                                                  style=" border-left: 1px solid #E3E3E3 !important; border-right: 1px solid #E3E3E3 !important; border-top: 1px solid #E3E3E3 !important; height: auto !important;" ></textarea>
                                            </div>
                                        </div>
                                        <div class="portlet-title row-margin">
                                            <div class="caption">
                                                <span class="caption-subject bold uppercase theme-text-color">Assign to Session</span>
                                            </div>
                                        </div>
                                        <div class="row row-margin">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label for="form_control_1">Select Session : </label>
                                                    <input class="form-control" type="text">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btndropborder dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;"> Longetivity & Life</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;"> Longetivity & Life</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;"> Longetivity & Life</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </form>
                                <!-- END FORM-->
                            <div class="row row-margin">
                                <div class="col-md-4">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject bold uppercase theme-text-color">Profile Photo</span>
                                        </div>
                                    </div>
                                    <form class="dropzone dropzone-file-area" id="profilephoto" style="width: 120px; height: 120px; border-radius: 50%;"> </form>
                                </div>
                                <div class="col-md-7">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject bold uppercase theme-text-color">Documentation</span>
                                        </div>
                                    </div>
                                    <form class="dropzone dropzone-file-area" id="documentation" style="height: 100px;"> </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
   <script>
        //DropzoneJS snippet - js
        $.getScript('assets/layouts/layout2/scripts/dropzone.js',function(){
          // instantiate the uploader
          $('#profilephoto').dropzone({ 
            url: "upload",
            maxFilesize: 100,
            paramName: "uploadfile",
            maxThumbnailFilesize: 5,
            init: function() {
              
              //this.on('success', function(file, json) {
              //});
              //
              //this.on('addedfile', function(file) {
              //  
              //});
              //
              //this.on('drop', function(file) {
              //  
              //}); 
            }
          });
        });
        
        
        // documentation DropzoneJS snippet - js
        $.getScript('assets/layouts/layout2/scripts/dropzone.js',function(){
          // instantiate the uploader
          $('#documentation').dropzone({ 
            url: "upload",
            maxFilesize: 100,
            paramName: "uploadfile",
            maxThumbnailFilesize: 5,
            init: function() {
              
              //this.on('success', function(file, json) {
              //});
              //
              //this.on('addedfile', function(file) {
              //  
              //});
              //
              //this.on('drop', function(file) {
              //  
              //}); 
            }
          });
        });
   </script>