<div class="tab-pane" id="tab4">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <span class="caption-subject bold uppercase">Manage Exhibitors</span>
                                    </div>
                                    <div class="actions" style="margin-top:0px;">
                                        <a class="btn green"><img src="assets/img/add_selection.png"> Add Exhibitor</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Search :</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="row row-margin">
                                    <div class="col-md-12">
                                        <div class="m-grid m-grid-demo">
                                            <div class="m-grid-row">
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-1 col-right-border ">
                                                    <img src="assets/img/schedule.png" alt="">
                                                </div>
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-md-9 col-padding">
                                                    <h4 style="margin-top: 0px;margin-bottom: 0px;" class="color">Facebook Inc.</h4>
                                                    <p style="margin-top: 0px;margin-bottom: 0px;">
                                                        <span>Booth: 034</span> &nbsp;&nbsp; <span>Phone: 448 556 6633</span> &nbsp;&nbsp; <span>Email: email@facebook.com</span>
                                                    </p>
                                                </div>
                                                <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-2 col-left-border">
                                                   <div>
                                                        <a><img src="assets/img/edit.png"></a>
                                                        <a><img src="assets/img/close.png"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>