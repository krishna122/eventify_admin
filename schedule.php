<div class="tab-pane" id="tab2">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
             <span class="caption-subject bold uppercase">Manage Schedule</span>
        </div>
        <div class="actions" style="margin-top:0px;">
             <a class="btn green"><i class="icon-cloud-download"></i> Import data</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <label>Search :</label>
            <input type="text" class="form-control">
        </div>
    </div>
    
    <div class="table-responsive">
        <table class="table table-bordered row-margin" style="border-bottom: 1px solid #e7ecf1;overflow: auto;">
            <thead>
                <tr>
                    <th style="width:7%;text-align: center;">
                        <a id="datepicker">
                            <img src="assets/img/calendar.png" >
                        </a>
                    </th>
                    <th style="text-align: center;background: #f8f8f8;"> 17/3/2017 </th>
                    <th style="text-align: center;border-top: 2px solid #323269 !important;"> 18/3/2017 </th>
                    <th style="text-align: center;background: #f8f8f8;"> 19/3/2017 </th>
                    <th style="text-align: center;background: #f8f8f8;"> 20/3/2017 </th>
                    <th style="text-align: center;background: #f8f8f8;"> 21/3/2017 </th>
                    <th style="text-align: center;background: #f8f8f8;"> 22/3/2017 </th>
                  </tr>
            </thead>
        </table>
    </div>
    
    <div class="cd-schedule loading row-margin">
	<div class="session">
		<ul>
			<li>
				<span>09:00</span>
			</li>
			<li>
				<span>09:30</span>
			</li>
			<li>
				<span>10:00</span>
			</li>
			<li>
				<span>10:30</span>
			</li>
			<li>
				<span>11:00</span>
			</li>
			<li>
				<span>11:30</span>
			</li>
			<li>
				<span>12:00</span>
			</li>
			<li>
				<span>12:30</span>
			</li>
			<li>
				<span>13:00</span>
			</li>
			<li>
				<span>13:30</span>
			</li>
			<li>
				<span>14:00</span>
			</li>
			<li>
				<span>14:30</span>
			</li>
			<li>
				<span>15:00</span>
			</li>
			<li>
				<span>15:30</span>
			</li>
			<li>
				<span>16:00</span>
			</li>
			<li>
				<span>16:30</span>
			</li>
			<li>
				<span>17:00</span>
			</li>
			<li>
				<span>17:30</span>
			</li>
			<li>
				<span>18:00</span>
			</li>
		</ul>
	</div> <!-- .session-time -->

	<div class="events">
		<ul>
			<li class="events-group">
				<div class="top-info">
					<a id="editorBtn"><i class="icon-pencil"></i></a>
					<span id="editor">Track1</span>
				</div>

				<ul>
					
					<li class="single-event" data-start="09:00" data-end="10:30" data-content="event-abs-circuit" data-event="event-1">
						<i class="icon-pencil"></i>
						<a href="#0">
							<em class="event-name">Technology Track 1</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="11:00" data-end="12:00" data-content="event-rowing-workout" data-event="event-2">
						<i class="icon-pencil"></i>
						<a href="#0">
							<em class="event-name">Technology Track 2</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="14:00" data-end="15:15"  data-content="event-yoga-1" data-event="event-3">
						<i class="icon-pencil"></i>
						<a href="#0">
							<em class="event-name">Technology Track 1</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info">
					<i class="icon-pencil"></i>
					<span>Track 2</span>
				</div>

				<ul>
					<li class="single-event" data-start="10:00" data-end="11:00"  data-content="event-rowing-workout" data-event="event-2">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="11:30" data-end="13:00"  data-content="event-restorative-yoga" data-event="event-4">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="13:30" data-end="15:00" data-content="event-abs-circuit" data-event="event-1">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="15:45" data-end="16:45"  data-content="event-yoga-1" data-event="event-3">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track 1</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info">
					<i class="icon-pencil"></i>
					<span>Track 3</span>
				</div>

				<ul>
					<li class="single-event" data-start="09:00" data-end="10:15" data-content="event-restorative-yoga" data-event="event-4">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track Yoga</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="10:45" data-end="11:45" data-content="event-yoga-1" data-event="event-3">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track 1</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:45"  data-content="event-rowing-workout" data-event="event-2">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="13:45" data-end="15:00" data-content="event-yoga-1" data-event="event-3">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track 1</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info">
					<i class="icon-pencil"></i>
					<span>Track 4</span>
				</div>

				<ul>
					<li class="single-event" data-start="09:30" data-end="10:30" data-content="event-abs-circuit" data-event="event-1">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:45" data-content="event-restorative-yoga" data-event="event-4">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="15:30" data-end="16:30" data-content="event-abs-circuit" data-event="event-1">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>

					<li class="single-event" data-start="17:00" data-end="18:30"  data-content="event-rowing-workout" data-event="event-2">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info">
					<i class="icon-pencil"></i>
					<span>Track 5</span>
				</div>

				<ul>
					<li class="single-event" data-start="10:00" data-end="11:00"  data-content="event-rowing-workout" data-event="event-2">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h4>
						</a>
					</li>

					<li class="single-event" data-start="12:30" data-end="14:00" data-content="event-abs-circuit" data-event="event-1">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track</em>
							
							<h5 class="color">Room : 301</h4>
						</a>
					</li>

					<li class="single-event" data-start="15:45" data-end="16:45">
						<i class="icon-pencil"></i>
						<a>
							<em class="event-name">Technology Track 1</em>
							
							<h5 class="color">Room : 301</h5>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="event-modal">
		<header class="header">
			<div class="content">
				<span class="event-date"></span>
				<h3 class="event-name"></h3>
			</div>

			<div class="header-bg"></div>
		</header>

		<div class="body">
			<div class="event-info"></div>
			<div class="body-bg"></div>
		</div>

		<a href="#0" class="close" data-dismiss=".event-modal">Close</a>
	</div>

	<div class="cover-layer"></div>
</div> <!-- .cd-schedule -->
    
<!-- BEGIN QUICK NAV -->
    <nav class="quick-nav">
        <a class="quick-nav-trigger" href="#0">
            <span aria-hidden="true"></span>
		</a>
		<ul>
			<li>
				<a data-toggle="modal" data-target="#addsession" class="active">
					<span>Add Session</span>
					<i class="icon-clock"></i>
				</a>
			</li>
			<li>
				<a>
					<span>Add Track</span>
					<i class="icon-target"></i>
				</a>
			</li>
		</ul>
		<span aria-hidden="true" class="quick-nav-bg"></span>
	</nav>
	<div class="quick-nav-overlay"></div>
<!-- END QUICK NAV -->
    
</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="assets/global/scripts/schedule.js"></script>
<script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>


<!-- Add Session Modal -->
<div id="addsession" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Session</h4>
      </div>
      <div class="modal-body">
		<div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<label>Session name :</label>
					<div class="input-icon input-icon-sm right">
						<i>30</i>
						<input class="form-control input-sm" type="text" name="">
					</div>
				</div>
				<div class="form-group">
					<label>Speaker :</label>
					<div class="input-icon input-icon-sm right">
						<select class="form-control">
							<option>Speaker 1</option>
							<option>Speaker 2</option>
							<option>Speaker 3</option>
							<option>Speaker 4</option>
							<option>Speaker 5</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label>Session time :</label>
					<div class="row">
						<div class="col-md-5">
							<div class="input-group bootstrap-timepicker timepicker" style="border: 1px solid #E3E3E3 !important;">
								<input id="timepicker1" type="text" class="form-control" style="border:0px !important">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-time"></i>
								</span>
							</div>
						</div>
						<div class="col-md-1">to</div>
						<div class="col-md-5">
							<div class="input-group bootstrap-timepicker timepicker" style="border: 1px solid #E3E3E3 !important;">
								<input id="timepicker2" type="text" class="form-control" style="border:0px !important">
								<span class="input-group-addon">
									<i class="glyphicon glyphicon-time"></i>
								</span>
							</div>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
				<div class="form-group">
					<label>Location :</label>
					<div class="input-icon input-icon-sm right">
						<input class="form-control input-sm" type="text" name="">
					</div>
				</div>
				<div class="form-group">
					<label>About :</label>
					<div class="input-icon input-icon-sm right">
						<textarea class="form-control" rows="3" col="3" style=" border-left: 1px solid #E3E3E3 !important; border-right: 1px solid #E3E3E3 !important; border-top: 1px solid #E3E3E3 !important; height: auto !important;"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label>Documents :</label>
					<div id="fileuploader">Upload</div>
				</div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>





<script>
	$(document).ready(function()
	{
		$("#fileuploader").uploadFile({
		url: "", // Server URL which handles File uploads
		method: "POST", // Upload Form method type POST or GET.
		enctype: "multipart/form-data", // Upload Form enctype.
		formData: null, // An object that should be send with file upload. data: { key1: 'value1', key2: 'value2' }
		returnType: null,
		allowedTypes: "*", // List of comma separated file extensions: Default is "*". Example: "jpg,png,gif"
		fileName: "file", // Name of the file input field. Default is file
		formData: {},
		dynamicFormData: function () { // To provide form data dynamically
			return {};
		},
		maxFileSize: -1, // Allowed Maximum file Size in bytes.
		maxFileCount: -1, // Allowed Maximum number of files to be uploaded
		multiple: true, // If it is set to true, multiple file selection is allowed. 
		dragDrop: true, // Drag drop is enabled if it is set to true
		autoSubmit: true, // If it is set to true, files are uploaded automatically. Otherwise you need to call .startUpload function. Default istrue
		showCancel: true,
		showAbort: true,
		showDone: false,
		showDelete: true,
		showError: true,
		showStatusAfterSuccess: true,
		showStatusAfterError: true,
		showFileCounter: true,
		fileCounterStyle: "). ",
		showProgress: true,
		nestedForms: true,
		showDownload:false,
		onLoad:function(obj){},
		onSelect: function (files) {
			return true;
		},
		onSubmit: function (files, xhr) {},
		onSuccess: function (files, response, xhr,pd) {},
		onError: function (files, status, message,pd) {},
		onCancel: function(files,pd) {},
		downloadCallback:false,
		deleteCallback: false,
		afterUploadAll: false,
		uploadButtonClass: "ajax-file-upload",
		dragDropStr: "<span><b>Drag &amp; Drop Files</b></span>",
		abortStr: "Abort",
		cancelStr: "Cancel",
		deletelStr: "x",
		doneStr: "Done",
		multiDragErrorStr: "Multiple File Drag &amp; Drop is not allowed.",
		extErrorStr: "is not allowed. Allowed extensions: ",
		sizeErrorStr: "is not allowed. Allowed Max size: ",
		uploadErrorStr: "Upload is not allowed",
		maxFileCountErrorStr: " is not allowed. Maximum allowed files are:",
		downloadStr:"Download",
		showQueueDiv:false,
		statusBarWidth:500,
		dragdropWidth:500
		});
	});
</script> 

<script>
//Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      orientation: 'bottom'
    });

	$('#timepicker1').timepicker();
	$('#timepicker2').timepicker();
	
	var editorBtn = document.getElementById('editorBtn');
	var element = document.getElementById('editor');

	editorBtn.addEventListener('click', function(e) {
	  e.preventDefault();

	  if (element.isContentEditable) {
	    // Disable Editing
	    element.contentEditable = 'false';
	    editorBtn.innerHTML = 'edit';
	    // You could save any changes here.
	  } else {
	    element.contentEditable = 'true';
	    editorBtn.innerHTML = 'ok';
	  }
	});
	
</script>    