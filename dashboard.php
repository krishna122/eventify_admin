<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Posts</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Photos</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Likes</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Comments</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>TOTAL Outreach</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>TOTAL Outreach</small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row row-margin">
    <div class="col-lg-8 col-xs-12 col-sm-12">
      <div class="portlet light ">
        <div class="portlet-title">
          <div class="caption caption-md">
            <span class="caption-subject bold">Notifications preview</span>
          </div>
        </div>
        <div class="portlet-body">
            <span class="font-dark bold">Latest notification</span>
          <div class="slimScrollDiv" style="position: relative; overflow-x: hidden; overflow-y: auto; width: 100%; height: 200px;">
              <div class="general-item-list">
                <div class="item">
                  <div class="item-head">
                    <div class="item-details">
                      <img class="item-pic rounded" src="assets/pages/media/users/avatar2.jpg">
                      <a href="" class="item-name primary-link">Lorem Ipsum is simply dummy text of the printing and typesetting</a>
                      <p class="item-label">4:40 pm - 11/20/2016 &nbsp;&nbsp;&nbsp; SENT</p>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="item-head">
                    <div class="item-details">
                      <img class="item-pic rounded" src="assets/pages/media/users/avatar2.jpg">
                      <a href="" class="item-name primary-link">Lorem Ipsum is simply dummy text of the printing and typesetting</a>
                      <p class="item-label">4:40 pm - 11/20/2016 &nbsp;&nbsp;&nbsp; SENT</p>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-xs-12 col-sm-12">
      <div class="portlet light ">
        <div class="portlet-title">
          <div class="caption caption-md">
            <span class="caption-subject bold">Check-ins</span>
          </div>
        </div>
        <div class="portlet-body">
            <span class="font-dark bold">Latest check-ins</span>
          <div class="slimScrollDiv" style="position: relative; overflow-x: hidden; overflow-y: auto; width: 100%; height: 200px;">
              <div class="general-item-list">
                <div class="item">
                  <div class="item-head">
                    <div class="item-details">
                      <img class="item-pic rounded" src="assets/pages/media/users/avatar2.jpg">
                      <a href="" class="item-name primary-link">Sarah o-conor</a>
                      <p class="item-label">4:40 pm - 11/20/2016 </p>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="item-head">
                    <div class="item-details">
                      <img class="item-pic rounded" src="assets/pages/media/users/avatar2.jpg">
                      <a href="" class="item-name primary-link">Sarah o-conor</a>
                      <p class="item-label">4:40 pm - 11/20/2016 </p>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>


<div class="portlet light">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold">Activity feed preview</span>
        </div>
    </div>

  <div class="portlet-body">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr style="border:none;">
            <th width="50%"> Latest Post 
            </th>
            <th> Outreach 
            </th>
            <th class="numeric"> Likes 
            </th>
            <th class="numeric"> Comments 
            </th>
            <th class="numeric"> Reported 
            </th>
            <th class="numeric"> 
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> 
              <div class="item-details">
                <img class="item-pic rounded" src="assets/pages/media/users/avatar4.jpg">
                <a href="" class="item-name primary-link">Nick Larson
                </a>
                <p style="margin-left: 44px; margin-top: -14px;" class="font-red-sunglo">The last speaker was out of this world
                </p>
              </div>
            </td>
            <td>100 
            </td>
            <td class="numeric"> 50 
            </td>
            <td class="numeric"> 300  
            </td>
            <td class="numeric"> 10000  
            </td>
            <td class="numeric">
              <center>
                <a class="btn btn-sm green"> HIDE</a>
              </center>
            </td>
          </tr>
          <tr>
            <td> 
              <div class="item-details">
                <img class="item-pic rounded" src="assets/pages/media/users/avatar4.jpg">
                <a href="" class="item-name primary-link">Nick Larson
                </a>
                <p style="margin-left: 44px; margin-top: -14px;" class="font-red-sunglo">The last speaker was out of this world
                </p>
              </div>
            </td>
            <td>100 
            </td>
            <td class="numeric"> 50 
            </td>
            <td class="numeric"> 300  
            </td>
            <td class="numeric"> 10000  
            </td>
            <td class="numeric">
              <center>
                <a class="btn btn-sm green"> HIDE</a>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>