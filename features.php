<div class="row">
    <div class="col-md-9">
        <div class="portlet light bordered">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="#" id="form_sample_3" novalidate="novalidate">
                    <div class="form-body">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                               <span class="caption-subject bold uppercase">BASIC DETAILS</span>
                            </div>
                            <div class="actions">
                                <a class="btn btn-sm green"> Submit</a>
                            </div>
                         </div>
                        
                        <div class="form-group">
                            <label>Event name :</label>
                            <div class="input-icon input-icon-sm right">
                              <i>30</i>
                              <input class="form-control input-sm" type="text" class="form-control" name="eventname"> 
                            </div>
                          </div>
                        
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <span class="caption-subject bold uppercase">Styling</span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                     <label for="form_control_1">Header : </label>
                                    <input class="form-control" type="text">
                                    <div class="input-group-btn">
                                      <button type="button" class="btn btndropborder dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-angle-down">
                                        </i>
                                      </button>
                                      <ul class="dropdown-menu pull-right">
                                        <li>
                                          <a href="javascript:;"> Text-based 
                                          </a>
                                        </li>
                                        <li>
                                          <a href="javascript:;"> Text-based
                                          </a>
                                        </li>
                                        <li>
                                          <a href="javascript:;"> Text-based
                                          </a>
                                        </li>
                                      </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <label for="form_control_1">Color : </label><br>
                                    <a href="#form_modal3" data-toggle="modal">
                                        <div class="btn-group btn-group-solid">
                                            <button type="button" class="btn bg-font-blue" style="background: #3598dc;">#3598dc</button>
                                            <button type="button" class="btn bg-font-blue" style="background: #4B77BE;">#4B77BE</button>
                                            <button type="button" class="btn bg-font-blue" style="background: #666666;">#666666</button>
                                            <button type="button" class="btn bg-font-blue" style="background: #1BBC9B;">#1BBC9B</button>
                                            <button type="button" class="btn bg-font-blue" style="background: #22313F;">#22313F</button>
                                        </div>
                                     </a>
                                </div>
                            </div>
                        </div>
                        <p></p>
                        <div class="form-group">
                            <label>App name :</label>
                            <div class="input-icon input-icon-sm right">
                              <i>17</i>
                              <input class="form-control input-sm" type="text" class="form-control" name="eventname"> 
                            </div>
                          </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Event date :</label>
                                    <div class="input-icon input-icon-sm right">
                                        <i class="fa fa-calendar"></i>
                                        <input class="form-control input-sm date-picker" id="datepicker" type="text" name="eventdate"> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Event location :</label>
                                    <div class="input-icon input-icon-sm right">
                                        <i class="icon-pointer"></i>
                                        <input class="form-control input-sm" type="text" class="form-control" name="eventdate"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="portlet-title row-margin">
                            <div class="caption font-red-sunglo">
                                <span class="caption-subject bold uppercase">App Menu Theme</span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="">
                                    <span style="float:left">Side-bar (Default)</span>
                                    <span style="float:right">
                                        <label class="switch">
                                            <input type="checkbox" checked="True">
                                            <div class="slider round" style="margin: 0px 0px !important;"></div>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <span style="float:left">Tab-bar</span>
                                    <span style="float:right">
                                        <label class="switch">
                                            <input type="checkbox">
                                            <div class="slider round" style="margin: 0px 0px !important;"></div>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="portlet-title row-margin">
                            <div class="caption font-red-sunglo">
                                <span class="caption-subject bold uppercase">App Features</span>
                                <span style="color: #FF885D;font-size: 10px;">
                                    &nbsp;<i class="icon-question"></i>
                                    Heighlight all the tabs/content you'd like to have it in the app
                                </span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/schedule.png" alt="">
                                            <h4>Schedule <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/map.png" alt="">
                                            <h4>Map <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/exhibitors.png" alt="">
                                            <h4>Exhibitors <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/sponsors.png" alt="">
                                            <h4>Sponsors <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double">
                                        <div class="tile-body">
                                            <img src="assets/img/about.png" alt="">
                                            <h4>About <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/news.png" alt="">
                                            <h4>News <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/interactivemap.png" alt="">
                                            <h4>Social Media <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/networking.png" alt="">
                                            <h4>Networking <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/speakers.png" alt="">
                                            <h4>Speakers <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="tiles">
                                  <div class="tile double active">
                                        <div class="tile-body">
                                            <img src="assets/img/web.png" alt="">
                                            <h4>Web View <sup><a><i class="icon-pencil color"></i></a></sup> </h4>
                                            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </form>
               <!-- END FORM-->
            </div>
        </div>
    </div>
    <!--<div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading text-center">
                <span style="float: left;"><img src="assets/img/menu.png"></span>
                <span>Schedule</span>
            </div>
            <div class="panel-body text-center">
                <span style="float: left;"><img src="assets/img/next.png"></span>
                <span>DECEMBER</span>
                <span style="float: right;"><img src="assets/img/prv.png"></span>
                <div>
                    <img src='assets/img/date-feature.png' style="width: 100%;">
                </div>
            </div>
            <div class="panel-footer">
                
            </div>
        </div>
    </div>-->
    <div class="col-md-3 preview-size">
<!--
        <div class="navbar-preview" style="margin-bottom:-20px;">
                <a style="color:transparent; font-size:20px; float:left;">
                    <i class="fa fa-bars"></i>
                </a> 
                <center> 
                    <a style="color:transparent; font-size:20px;">
                        Speakers
                    </a> 
                </center>
                <a style="color:transparent; font-size:20px; float:right;margin-top: -26px;">
                    <i class="fa fa-search" style="margin-right:15px;"></i>
                    <i class="fa fa-bookmark"></i>
                </a>
        </div>
-->
        <!--<div class="thumbnailP" style="background:url('assets/img/schedule.jpg');">
            <div class="panel-body" style="background:#f5f5f5;width:60%;box-shadow: 2px 0px 5px #888888;padding: 0px;padding-top: 40px;">
            
                <p class="preview-feature-p">
                    <img src="assets/img/menuspeakersicon.png">
                    &nbsp;&nbsp;&nbsp; Speakers
                </p>
                <p class="preview-feature-p" style="background:#f5f5f5;">
                    <img src="assets/img/menuexhibitorsicon.png">
                    &nbsp;&nbsp;&nbsp; Exhibitors
                </p>
                <p class="preview-feature-p">
                    <img src="assets/img/menumapicon.png">
                        &nbsp;&nbsp;&nbsp; Maps
                    </p>
                
                    <p class="preview-feature-p">
                        <img src="assets/img/menusponsorsicon.png">
                        &nbsp;&nbsp;&nbsp; Sponsors
                    </p>
                
                    <p class="preview-feature-p">
                        <img src="assets/img/menunewsicon.png">
                        &nbsp;&nbsp;&nbsp; News
                    </p>
                
                    <p class="preview-feature-p">
                        <img src="assets/img/networking.png">
                        &nbsp;&nbsp;&nbsp; Networking
                    </p>
                
                    <p class="preview-feature-p">
                        <img src="assets/img/menuscheduleicon.png">
                        &nbsp;&nbsp;&nbsp; Schedule
                    </p>
                    
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-15 col-sm-3 text-center">
                        <a href="#" class="color">
                            <img src="assets/img/menuspeakersicon.png">
                            <br>Speakers
                        </a>
                    </div>
                    <div class="col-md-15 col-sm-3 text-center">
                        <a href="#" class="color">
                            <img src="assets/img/menuexhibitorsicon.png">
                            <br>Exhibitors
                        </a>
                    </div>
                    <div class="col-md-15 col-sm-3 text-center">
                        <a href="#" class="color">
                            <img src="assets/img/menumapicon.png">
                            <br>Maps
                        </a>
                    </div>
                    <div class="col-md-15 col-sm-3 text-center">
                        <a href="#" class="color">
                            <img src="assets/img/menusponsorsicon.png">
                            <br>Sponsors
                        </a>
                    </div>
                    <div class="col-md-15 col-sm-3 text-center">
                        <div class="btn-group dropup">
                            <a href="#" class="color dropdown-toggle bounceInUp" data-toggle="dropdown" aria-expanded="false">
                                <img src="assets/img/menumoreicon.png">
                                <br>More
                            </a>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li class="active">
                                    <a href="#tab1" data-toggle="tab"><img src="assets/img/menuspeakersicon@3x.png" alt=""> &nbsp; Speakers</a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab"><img src="assets/img/schedule.png" alt=""> &nbsp; Schedule</a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab"><img src="assets/img/about.png" alt=""> &nbsp; About</a>
                                </li>
                                <li>
                                    <a href="#tab4" data-toggle="tab"><img src="assets/img/exhibitors.png" alt=""> &nbsp; Exhibitors</a>
                                </li>
                                <li>
                                    <a href="#tab5" data-toggle="tab"><img src="assets/img/map.png" alt=""> &nbsp; Map</a>
                                </li>
                                <li>
                                    <a href="#tab6" data-toggle="tab"><img src="assets/img/interactivemap.png" alt=""> &nbsp; Sponsors</a>
                                </li>
                                <li>
                                    <a href="#tab7" data-toggle="tab"><img src="assets/img/news.png" alt=""> &nbsp; Social feed</a>
                                </li>
                                <li>
                                    <a href="#tab8" data-toggle="tab"><img src="assets/img/web.png" alt=""> &nbsp; Web view</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
</div>



<!--modal for theme color-->

<div id="form_modal3" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="about__inner">
                        <div class="about__slider js-hint-slider slick-initialized slick-slider">
                            <div aria-live="polite" class="slick-list draggable">
                                <div class="slick-track"  role="listbox">
                                    <div class="about__slide about__slide_first slick-slide slick-current slick-active">
                                        <div class="about__slide-in">
                                            <div class="about__img">
                                                <div class="col-md-4" style="border: solid 1px #e3e3e3;height:514px; width:436px;"></div>
                                            </div>
                                            <div class="about__hint about__hint_first is-right">
                                                <label for="form_control_1">Contrast : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="about__hint about__hint_second">
                                                <label for="form_control_1">Contrast : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="about__hint about__hint_third is-right">
                                                <label for="form_control_1">Contrast : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                            <div class="about__hint about__hint_fourth">
                                                <label for="form_control_1">Contrast : </label>
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
//Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
    
//color picker
    $('.colorpicker').colorpicker();
</script>
