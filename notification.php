<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">2448</span>
                    </h3>
                    <small>Notification sent</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div class="dashboard-stat2 ">
            <form role="form">
                <div class="form-body">
                    <div class="form-group form-md-line-input has-error">
                        <div class="input-group">
                            <div class="input-group-control">
                                <input type="text" class="form-control" placeholder="Broadcast a new notification..">
                            </div>
                            <span class="input-group-btn btn-right">
                                <a type="button" class="btn btn-primary green"> SEND OUT</a>
                            </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="portlet light"  style="margin-top: 15px;">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Notification Management</span>
        </div>
    </div>

  <div class="portlet-body">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr style="border:none;">
            <th width="40%"> Latest notifications sent
            </th>
            <th>  
            </th>
            <th class="numeric"> Rate
            </th>
            <th class="numeric"> 
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> 
              <div class="item-details">
                <a class="item-name primary-link">Lorem Ipsum is simply dummy text of the printing</a>
                <p class="font-red-sunglo">4:40 pm - 10/10/2016 <span>SENT</span></p>
              </div>
            </td>
            <td> &nbsp;
            </td>
            <td class="numeric"> 70%
            </td>
            <td class="numeric">
              <center>
                <a class="btn-group" style="font-size: 25px;">
                  <img src="assets/img/cross.png">
                </a>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
