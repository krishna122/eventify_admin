<div class="tab-pane" id="tab7">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Manage Social Feed</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a type="button" class="btn btn-Linkedin" data-toggle="modal" data-target="#Linkedin">
                    <i class="fa fa-linkedin"></i> Add Linkedin    
                </a>
                <a type="button" class="btn btn-Instagram" data-toggle="modal" data-target="#Instagram">
                    <i class="fa fa-instagram"></i> Add Instagram    
                </a>
                <a type="button" class="btn btn-Facebook" data-toggle="modal" data-target="#facebook">
                    <i class="fa fa-facebook"></i> Add Facebook
                </a>
                <a type="button" class="btn btn-Twitter" data-toggle="modal" data-target="#twitter">
                    <i class="fa fa-twitter"></i> Add Twitter
                </a>
            </div>
        </div>
        <div class="col-md-12">
            <label>Search :</label>
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top" style="margin-top: -31px;">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top" style="margin-top: -31px;">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Linkedin Modal -->
<div id="Linkedin" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Instagram Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Your Linkedin Link</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label>Link : </label>
                <input type="text" class="form-control">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save</button>
      </div>
    </div>

  </div>
</div>

<!-- Instagram Modal -->
<div id="Instagram" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Instagram Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Your Instagram Link</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label>Link : </label>
                <input type="text" class="form-control">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save</button>
      </div>
    </div>

  </div>
</div>

<!-- Facebook Modal -->
<div id="facebook" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Facebook Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Your Facebook Link</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label>Link : </label>
                <input type="text" class="form-control">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save</button>
      </div>
    </div>

  </div>
</div>

<!-- Twitter Modal -->
<div id="twitter" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Twitter Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Your Twitter Link</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <label>Link : </label>
                <input type="text" class="form-control">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default">Save</button>
      </div>
    </div>

  </div>
</div>