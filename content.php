<div class="row">
    <div class="col-md-9">
        <form class="form-horizontal form-row-seperated" action="#">
            <!-- BEGIN TAB PORTLET-->
            <div class="portlet light " style="padding: 0px 0px 15px 0px; height: 800px; width: 100%; overflow: auto;">
                <div class="portlet-body" style="padding-top: 0px;">
                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li class="dropdown pull-right tabdrop">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                    <img src="assets/img/more.png" alt="" style="height:25px; width:25px;">
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab">
                                            <img src="assets/img/speakers.png" alt="" style="height:25px; width:25px;"> Speakers
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab">
                                            <img src="assets/img/schedule.png" alt="" style="height:25px; width:25px;"> Schedule
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab">
                                            <img src="assets/img/about.png" alt="" style="height:25px; width:25px;"> About
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab">
                                            <img src="assets/img/exhibitors.png" alt="" style="height:25px; width:25px;"> Exhibitors
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab5" data-toggle="tab">
                                            <img src="assets/img/map.png" alt="" style="height:25px; width:25px;"> Map
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab6" data-toggle="tab">
                                            <img src="assets/img/interactivemap.png" alt="" style="height:25px; width:25px;"> Sponsors
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab7" data-toggle="tab">
                                            <img src="assets/img/news.png" alt="" style="height:25px; width:25px;"> Social feed
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab8" data-toggle="tab">
                                            <img src="assets/img/web.png" alt="" style="height:25px; width:25px;"> Web view
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">
                                    <img src="assets/img/speakers.png" alt="" style="height:25px; width:25px;"> Speakers
                                </a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab">
                                    <img src="assets/img/schedule.png" alt="" style="height:25px; width:25px;"> Schedule
                                </a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab">
                                    <img src="assets/img/about.png" alt="" style="height:25px; width:25px;"> About
                                </a>
                            </li>
                            <li>
                                <a href="#tab4" data-toggle="tab">
                                    <img src="assets/img/exhibitors.png" alt="" style="height:25px; width:25px;"> Exhibitors
                                </a>
                            </li>
                            <li>
                                <a href="#tab5" data-toggle="tab">
                                    <img src="assets/img/map.png" alt="" style="height:25px; width:25px;"> Map
                                </a>
                            </li>
                            <!--<li>
                                <a href="#tab6" data-toggle="tab">
                                    <img src="assets/img/interactivemap.png" alt="" style="height:25px; width:25px;">  Sponsors
                                </a>
                            </li>
                            <li>
                                <a href="#tab7" data-toggle="tab">
                                    <img src="assets/img/news.png" alt="" style="height:25px; width:25px;">  Social feed
                                </a>
                            </li>
                            <li>
                                <a href="#tab8" data-toggle="tab">
                                    <img src="assets/img/web.png" alt="" style="height:25px; width:25px;">  Web view
                                </a>
                            </li>-->
                        </ul>
                        <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
                            
                            <!--Speakers-->
                            <?php include 'content-speaker.php'; ?>
                            
                            <!--Schedule-->
                            <?php include 'schedule.php'; ?>
                            
                            <!--About-->
                            <?php include 'about.php'; ?>
                            
                            <!--Exhibitors-->
                            <?php include 'exhibitors.php'; ?>
                            
                            <!--Map-->
                            <?php include 'map.php'; ?>
                            
                            <!--Sponsors-->
                            <?php include 'sponsor.php'; ?>
                            
                            <!--Social Feed-->
                            <?php include 'socialfeed.php'; ?>
                            
                            <!--Web View-->
                            <?php include 'webview.php'; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TAB PORTLET-->
        </form>
    </div>
    
   <!-- <div class="col-md-3 preview-size"> -->
        <!--Content-Speaker-Preview-->
        <?php //include 'preview-contentspeaker.php'; ?>
        
        <!--Content-About-Preview-->
        <?php //include 'preview-about.php'; ?>
        
        <!--Content-Sponsors-Preview-->
        <?php //include 'preview-sponsors.php'; ?>
        
        <!--Content-Webview-Preview-->
        <?php //include 'preview-Webview.php'; ?>
<!--    </div>-->
    
    <div class="col-md-3">
        <div class="portlet light bordered" style="height: 730px; width: 100%;padding:0px; overflow:auto;">
            <div class="portlet-title" style="border:1px solid #eee!important;">
                <div class="navbar-preview" style="padding:15px !important;">
                    <a style="color:#818592; font-size:20px; float:left;">
                        <i class="fa fa-bars"></i>
                    </a> 
                    <center> 
                        <a style="color:#818592; font-size:20px;">
                            Web View
                        </a> 
                    </center>
                    <a style="color:#818592; font-size:20px; float:right;margin-top: -26px;">
                        <i class="fa fa-search" style="margin-right:15px;"></i>
                        <i class="fa fa-bookmark"></i>  
                    </a>
                </div>
            </div>
            <div class="portlet-body" style="padding:15px !important;">
                <!--Content-Speaker-Preview-->
                <?php //include 'preview-contentspeaker.php'; ?>

                <!--Content-About-Preview-->
                <?php //include 'preview-about.php'; ?>

                <!--Content-Sponsors-Preview-->
                <?php //include 'preview-sponsors.php'; ?>

                <!--Content-Webview-Preview-->
                <?php include 'preview-Webview.php'; ?>
            </div>
        </div>
    </div>
    
</div>
