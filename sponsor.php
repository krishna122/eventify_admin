<div class="tab-pane" id="tab6">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <span class="caption-subject bold uppercase">Manage Sponsors</span>
        </div>
        <div class="actions" style="margin-top:0px;">
            <a class="btn green"><i class="icon-map"></i> Add Sponsors</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-5">
            <label>Search :</label>
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="row row-margin">
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top" style="margin-top: -31px;">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tiles">
                <div class="tile double">
                    <div class="tile-body">
                        <img src="assets/img/schedule.png" alt="" class="item-pic rounded">
                        <h4>Fernando Arbulu</h4>
                        <p> UX designer | Farbulu.com</p>
                        <div class="pull-right icon-top">
                            <a><img src="assets/img/edit.png"></a>
                            <a><img src="assets/img/close.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    